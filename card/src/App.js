import React,{useState} from "react";
import logo from './logo.svg';
import './App.css';
import CardBlock from "./Components/CardBlock/CardBlock.js";

function App() {
    const[text,setText] = useState('');
  return (
    <div className="App">
        <CardBlock/>
    </div>
  );
}

export default App;
