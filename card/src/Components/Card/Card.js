import React from "react";
import './Card.css'
const Card = ({text,setText}) => {

    return(
            <div className="card">
                <p className="item">{text}</p>
            </div>
    );
}

export default Card;